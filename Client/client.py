import socket

host = "server"
port = 12345

print("Client starting")

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host, port))

print("Connected to server")

while True:
    byte = s.recv(1) # waits one byte
    text = byte.decode() # convert bytes to string object
    print(text, end="", flush=True) 

    # end="", new char- not on a new line
    # flush=True, not buffered output
