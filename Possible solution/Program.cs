﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppEE
{
    class Program
    {
        static void Main(string[] args)
        {
            Client();
        }
        static void Client()
        {
            try
            {
                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Parse("[SERVER IP]"), 12345);

                Socket s = new Socket(AddressFamily.InterNetwork,SocketType.Stream, ProtocolType.Tcp);

                try
                {
                    s.Connect(localEndPoint);

                    Console.WriteLine("Socket is connected to: {0} ", s.RemoteEndPoint.ToString());

                    byte[] textReceived = new byte[1024];

                    int byteReceived = s.Receive(textReceived);

                    Console.WriteLine("Text from server: {0}", Encoding.ASCII.GetString(textReceived, 0, byteReceived));
                }
                catch (Exception e)
                {
                    Console.WriteLine("Unexpected excep : {0}", e.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
