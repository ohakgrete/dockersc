# DockerSC

## Name
Server and client communication with Docker

## Description
If the server and the client are connected then it is possible to write a text to the server and at the same that text is immediately visible on the client side.

## Usage
Open 3 terminals and insert the following commands.
1. terminal use (commands):
```
docker-compose build
docker-compose up
```

2. terminal use (commands):
```
docker-compose exec server /bin/sh
python server.py
```

3. terminal use (commands):
```
docker-compose exec client /bin/sh
python client.py
```


After that it is possible to enter any text to the second terminal (server) and the client prints it out immediately.

Output from the server (example, I wrote "hello"):

```
PS C:\Users\Kasutaja\dockerSC> docker-compose exec server /bin/sh
# python server.py
Server starting
Waiting for client to connect
Client connected from ('172.19.0.3', 43116)
hello
```

And at the same time you can see the text in real time on the client side.

Output from the client (example):
```
PS C:\Users\Kasutaja\dockerSC> docker-compose exec client /bin/sh
# python client.py
Client starting
Connected to server
hello
```

## Author
Grete Ohak

## Project status
Ready but not as expected.
