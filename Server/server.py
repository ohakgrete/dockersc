import socket
from getch import getche
# library that helps to get unbuffered data better

port = 12345

print("Server starting")

# AF_INET -IPv4 Internet protocols
# SOCK_STREAM provides sequenced, reliable, two-way, connection-based byte streams

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(("", port)) # assigns an IPAdress (all, equal to 0.0.0.0) and a port number to a socket
s.listen(1) # one client to listen

print("Waiting for client to connect")

# to accept a connection request from a client
c, addr = s.accept() 

print("Client connected from", addr)

while True:
    # msg = input() + "\n" not soft real time, needs enter to send data, so I left it
    msg = getche()
    c.send(msg.encode())
